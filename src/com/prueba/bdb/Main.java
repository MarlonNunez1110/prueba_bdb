package com.prueba.bdb;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hola mundo");
		System.out.println("Suma: " + suma(1,2));
		System.out.println("Resta: " + resta(1,2));
		System.out.println("Tablas de multiplicar");
		tablasMultiplicar();
	}
	
	private static int suma(int a, int b) {
		return a + b;
	}

	private static int resta(int a, int b) {
		return a - b;
	}
	
	private static void tablasMultiplicar() {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				System.out.println(i + " * " + j + " : " + i * j);
			}
		}
	}
}
